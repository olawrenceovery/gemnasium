import Dependencies._

ThisBuild / organization := "com.example"
ThisBuild / scalaVersion := "2.12.6"
ThisBuild / version := "0.1.0-SNAPSHOT"

lazy val common = Seq(
  libraryDependencies += scalaTest % Test,
  libraryDependencies += "com.fasterxml.jackson.core" % "jackson-databind" % "2.9.2" % "compile",
)

lazy val root = (project in file(".")).
  settings(
    common,
    libraryDependencies += "org.mozilla" % "rhino" % "1.7.10" % "compile",
  )
  .aggregate(proj1, proj2)

lazy val proj1 = (project in file("proj1")).
  settings(
    common,
    libraryDependencies += "org.apache.geode" % "geode-core" % "1.1.1" % "compile"
  )

lazy val proj2 = (project in file("proj2")).
  settings(
    common,
    libraryDependencies += "com.fasterxml.jackson.core" % "jackson-databind" % "2.9.4" % "compile",
  )
