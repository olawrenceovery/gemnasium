#!/usr/bin/env python3

import sys
import logging
import json
from json import JSONDecodeError
import ijson
from jsonschema import validate, ValidationError
from packaging.specifiers import SpecifierSet, InvalidSpecifier, LegacySpecifier, Specifier


class BestEffortSpecifier(LegacySpecifier):
    def __init__(self, specifier):
        super().__init__(specifier)
        self.specifiers = {super()}
        try:
            self.specifiers.add(Specifier(specifier))
        except InvalidSpecifier:
            pass

    def contains(self, item, prereleases=None):
        return satisfies(lambda range: range.contains(item), self.specifiers)


# a helper class that use best effort version matching includig PEP386 and PEP
class BestEffortSpecifierSet(SpecifierSet):
    def __init__(self, specifiers=""):
        specifiers = [s.strip() for s in specifiers.split(",") if s.strip()]
        parsed = set()
        for specifier in specifiers:
             parsed.add(BestEffortSpecifier(specifier))
        self._specs = frozenset(parsed)
        self._prereleases = True


def satisfies(func, seq):
    """Return true if f(item) evals to true."""
    for item in seq:
        if func(item):
            return True
    return False


USAGE = 'rangecheck.py <JSON input file>'

logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)

if len(sys.argv) != 2:
    logging.error(USAGE)
    sys.exit(1)

JSON_INPUT_FILE = sys.argv[1]
JSON_SCHEMA = """
{
  "definitions": {},
  "$schema": "http://json-schema.org/draft-07/schema#",
  "$id": "http://example.com/root.json",
  "type": "array",
  "title": "The Root Schema",
  "items": {
    "$id": "#/items",
    "type": "object",
    "title": "The Items Schema",
    "required": [
      "range",
      "version"
    ],
    "properties": {
      "range": {
        "$id": "#/items/properties/range",
        "type": "string",
        "title": "Version Range Definition",
        "pattern": "^(.*)$"
      },
      "error": {
        "$id": "#/items/properties/error",
        "type": "string",
        "title": "Error Message",
        "pattern": "^(.*)$"
      },
      "version": {
        "$id": "#/items/properties/version",
        "type": "string",
        "title": "The Version Schema",
        "pattern": "^(.*)$"
      },
      "satisfies": {
        "$id": "#/items/properties/satisfies",
        "type": "boolean",
        "title": "true if version lies within range, false otherwise",
        "pattern": "^(true|false)$"
      }
    }
  }
}
"""

try:
    FILE_HANDLE = open(JSON_INPUT_FILE, 'r')
    validate(json.load(open(JSON_INPUT_FILE, 'r')), json.loads(JSON_SCHEMA))
except IOError as io_exception:
    logging.error(str(io_exception))
    sys.exit(1)
except ValidationError as validation_exception:
    logging.error(validation_exception.message)
    sys.exit(1)
except JSONDecodeError as decode_exception:
    logging.error(decode_exception.msg)
    sys.exit(1)

PARSER = ijson.parse(FILE_HANDLE)

# range is a list of version specifiers
RANGE = []
VERSION = None
ADD_COMMA = False
OUT = {}

sys.stdout.write('[\n')

for prefix, event, value in PARSER:
    if prefix.endswith('version'):
        OUT['version'] = value
        if len(value) == 0:
            OUT['error'] = 'version not specified'
        else:
            VERSION = value
    elif prefix.endswith('range'):
        OUT['range'] = value
        try:
            if len(value) == 0:
                OUT['error'] = 'range not specified'
            else:
                version_array = value.replace(' ', '').split('||')
                RANGE = list(map(lambda x: BestEffortSpecifierSet(x), version_array))
        except InvalidSpecifier as invalid_specifier_exception:
            OUT['error'] = str(invalid_specifier_exception)
            RANGE = None
    elif event == 'end_map':
        if 'error' not in OUT.keys():
            OUT['satisfies'] = satisfies(lambda range: range.contains(VERSION), RANGE)
        if ADD_COMMA:
            sys.stdout.write(',\n')
        sys.stdout.write(json.dumps(OUT, ensure_ascii=False))
        RANGE, VERSION = None, None
        OUT = {}
        ADD_COMMA = True

sys.stdout.write('\n]')
sys.exit(0)
