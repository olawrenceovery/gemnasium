package gem

import "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v3/vrange/cli"

func init() {
	cli.Register("gem", "gem/vrange.rb")
}
