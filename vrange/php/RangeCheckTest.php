<?php

declare(strict_types=1);

# Run with ./vendor/bin/phpunit --bootstrap vendor/autoload.php RangeCheckTest

use PHPUnit\Framework\TestCase;

final class RangeCheckTest extends TestCase
{
    public function testWellFormedJsonFile(): void
    {
        $output = "";
        $exitcode = 0; 
        exec('./rangecheck.php tests/simple_in.json', $output, $exitcode);
        $expectation = file_get_contents('tests/simple_out.json');
        $this->assertEquals(implode("\n", $output), $expectation);
        $this->assertEquals($exitcode, 0);
    }

    public function testDictionaryJsonFile(): void
    {
        $output = "";
        $exitcode = 0; 
        exec('./rangecheck.php tests/dictionary.json', $output, $exitcode);
        $this->assertEquals(implode("\n", $output), "Malformed JSON file");
        $this->assertEquals($exitcode, 1);
    }

    public function testEmptyJsonFile(): void
    {
        $output = "";
        $exitcode = 0; 
        exec('./rangecheck.php tests/empty.json', $output, $exitcode);
        $this->assertEquals(implode("\n", $output), "Malformed JSON file");
        $this->assertEquals($exitcode, 1);
    }

    public function testNonExistentJsonFile(): void
    {
        $output = "";
        $exitcode = 0; 
        exec('./rangecheck.php tests/empty00.json', $output, $exitcode);
        $this->assertEquals(implode("\n", $output), "tests/empty00.json does not exist");
        $this->assertEquals($exitcode, 1);
    }

    public function testGemnasiumDbAdvisoryRanges(): void
    {
        $output = "";
        $exitcode = 0; 
        exec('./rangecheck.php tests/adb_ranges_in.json', $output, $exitcode);
        $expectation = file_get_contents('tests/adb_ranges_out.json');
        $this->assertEquals(implode("\n", $output), $expectation);
        $this->assertEquals($exitcode, 0);
    }
}
