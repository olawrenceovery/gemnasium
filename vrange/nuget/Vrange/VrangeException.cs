using System;

namespace Vrange
{
    public class VrangeException :Exception
    {
        public VrangeException(string name)
            : base(name)
        {
        }
    }
}