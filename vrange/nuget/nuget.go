package nuget

import (
	"runtime"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v3/vrange/cli"
)

func init() {
	cli.Register("nuget", "nuget/vrange-"+runtime.GOOS)
}
